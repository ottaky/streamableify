let observer = new MutationObserver(mutationsList => {
  try {

    let maybeVideo = mutationsList[0]
                     .addedNodes.item(0) // div.video-advanced
                     .childNodes[3]      // div
                     .childNodes[0]      // div.res-media-independent
                     .childNodes[0]      // div.video-advanced-container.res-media-movable.res-media-keep-visible
                     .childNodes[1]      // div.res-media-with-controls-wrapper
                     .childNodes[3]      // video.res-media-zoomable.res-media-rotatable

    if (   maybeVideo.nodeName === 'VIDEO'
        && maybeVideo.childNodes[0].src.match(/streamable/)) {
      maybeVideo.volume = 0.1
    }
  }
  catch(err) {}
});

observer.observe(
  document.getElementById('siteTable'),
  { subtree: true, childList: true }
)




